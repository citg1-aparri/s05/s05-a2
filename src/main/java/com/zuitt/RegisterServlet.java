package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3834552629113201265L;
	
	public void init() throws ServletException{
		System.out.println("RegisterServlet initialized.");	
		}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
		String firstName = req.getParameter("firstname");
		String lastName = req.getParameter("lastname");
		String email = req.getParameter("email");
		String phone = req.getParameter("phone");
		String knownFrom = req.getParameter("known_from");
		String bday = req.getParameter("birthdate");
		String type = req.getParameter("type");
		String desc = req.getParameter("desc");
		
		HttpSession session = req.getSession();
		
		session.setAttribute("firstname", firstName);
		session.setAttribute("lastname", lastName);
		session.setAttribute("email", email);
		session.setAttribute("phone", phone);
		session.setAttribute("knownFrom", knownFrom);
		session.setAttribute("bday", bday);
		session.setAttribute("type", type);
		session.setAttribute("desc", desc);
		
		res.sendRedirect("register.jsp");
	}
	
	public void destroy() {
		System.out.println("RegisterServlet destroyed.");	
		}
		
}
