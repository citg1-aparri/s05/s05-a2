<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home</title>
</head>
<body>
<h1> Welcome <%= session.getAttribute("fullname") %>!</h1>
<p>
	<%
		if(session.getAttribute("type").equals("Employer")){
			out.println("Welcome employer, you may now start browsing applicant profiles.");
		}else{
			out.println("Welcome applicant, you may now start looking for your career opportunity.");
		}
	 %>
</p>
</body>
</html>