<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Registration Confirmation</title>
</head>
<body>
	<h1>Registration Confirmation</h1>
	<p>First Name: <%= session.getAttribute("firstname") %></p>
	<p>Last Name: <%= session.getAttribute("lastname") %></p>
	<p>Phone: <%= session.getAttribute("phone") %></p>
	<p>Email: <%= session.getAttribute("email") %></p>
	<p>App Discovery: <%= session.getAttribute("knownFrom") %></p>
	<p>Date of Birth: <%= session.getAttribute("bday") %></p>
	<p>User Type: <%= session.getAttribute("type") %></p>
	<p>Description: <%= session.getAttribute("desc") %></p>
	<form action="login" method="post">
		<input type="submit" value="Submit"/>
	</form>
	
	<form action="index.jsp" >
		<input type="submit" value="Back"/>
	</form>
</body>
</html>