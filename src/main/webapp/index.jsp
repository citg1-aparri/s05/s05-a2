<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Servlet Job Finder App</title>
<style>
	div{
	margin:5px;
	}
</style>
</head>
<body>
	<div id=container>
		<h1>Welcome to Servlet Job Finder!</h1>
		<form action="register" method="POST">
		<div>
			<label for="firstname">First Name</label>
			<input type="text" name="firstname"/>
		</div>
		<div>
			<label for="lastname">Last Name</label>
			<input type="text" name="lastname"/>
		</div>
		<div>
			<label for="phone">Phone</label>
			<input type="tel" name="phone"/>
		</div>
		<div>
			<label for="email">Email</label>
			<input type="email" name="email"/>
		</div>
		<fieldset>
			<legend>How did you discover this app?</legend>
			<input type="radio" id="friends" name="known_from" value="friend" required />
			<label for="friends">Friends</label>
			<br>
			<input type="radio" id="soc_med" name="known_from" value="social media" required />
			<label for="soc_med">Social Media</label>
			<br>
			<input type="radio" id="others" name="known_from" value="others" required />
			<label for="others">Others</label>
		</fieldset>
		<div>
			<label for="birthdate">Date of birth</label>
			<input type="date" name="birthdate"/>
		</div>
		<div>
			<label for="type">Are you an employer or applicant?</label>
			<select id="usertype" name="type">
				<option value="" selected="selected">
					Select Type 
				</option>
				<option value="Applicant">
					Applicant 
				</option>
				<option value="Employer">
					Employer
				</option>
			</select>
		</div>
		<div>
			<label for="desc">Profile Description</label>
			<textarea name="desc" maxlength="500"></textarea>
		</div>
		<input type="submit" value="Register"/>
		</form>
	</div>
	
</body>
</html>